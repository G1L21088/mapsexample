package mx.gilsantaella.mapsexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnMapa;
    private Button btnParcial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setCasting();
        setActions();
    }

    private void setCasting() {
        btnMapa= (Button)findViewById(R.id.btnMapa);
        btnParcial = (Button)findViewById(R.id.btnParcial);
    }

    private void setActions() {
        btnMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(i);
            }
        });

        btnParcial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MapaParcial.class);
                startActivity(i);
            }
        });
    }
}
